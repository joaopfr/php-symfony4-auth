<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername("user1")
            ->setEmail("user1@a.com")
            ->setRoles("ROLE_USER");

        $pass = $this->passwordEncoder->encodePassword($user, "abc");
        $user->setPassword($pass);

        $manager->persist($user);
        $manager->flush();

        $admin = new User();
        $admin->setUsername("admin")
            ->setEmail("admin@a.com")
            ->setRoles("ROLE_ADMIN");

        $adminPass = $this->passwordEncoder->encodePassword($admin, "cde");
        $admin->setPassword($adminPass);

        $manager->persist($admin);
        $manager->flush();
    }
}
