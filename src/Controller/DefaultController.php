<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     * @Template("default/index.html.twig")
     */
    public function index()
    {
        return [
            'controller_name' => 'DefaultController',
        ];
    }

    /**
     * @return array
     *
     * @Route("/admin", name="admin")
     * @Template("default/admin.html.twig")
     */
    public function admin()
    {
        $texto = "Usuário comum";
        if ($this->isGranted("ROLE_ADMIN"))
        {
            $texto = "Usuário Administrador.";
        }
        return [
            "texto" => $texto
        ];
    }

    /**
     * @Route("/admin/dashboard", name="dashboard")
     * @Template("default/dashboard.html.twig")
     */
    public function dashboard()
    {
        return [];
    }

    /**
     * @Route("/admin/relatorios", name="relatorios")
     * @Template("default/relatorios.html.twig")
     */
    public function relatorios()
    {
        return [];
    }

    /**
     * @Route("/admin/login", name="login")
     * @Template("default/login.html.twig")
     */
    public function login(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        return [
            "error" => $error,
            "last_username" => $lastUsername
        ];
    }
}
